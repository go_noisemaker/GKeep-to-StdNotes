import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class TreeNode<T> {

    T value;

    TreeNode<T> parent;

    Collection<TreeNode<T>> children;

    public TreeNode(T value) {
        this.value = value;
        this.children = new ArrayList<>();
    }

    static <T> TreeNode<T> createRoot() {

        return new TreeNode<>(null);
    }

    void addChild(T value) {

        TreeNode<T> child = new TreeNode<>(value);
        addChild(child);
    }

    private void addChild(TreeNode<T> node) {

        node.parent = this;
        children.add(node);
    }

    Optional<TreeNode<T>> findSelfOrDescendantMatching(Predicate<T> predicate) {

        if (this.value != null && predicate.test(this.value)) {
            return Optional.of(this);
        } else {
            return this.children.stream()
                    .map(child -> child.findSelfOrDescendantMatching(predicate))
                    .flatMap(Optional::stream)
                    .findAny();
        }
    }

    int maxDepth() {
        return this.maxDepth(0);
    }

    private int maxDepth(int baseDepth) {

        return this.children.stream()
                .map(child -> child.maxDepth(baseDepth + 1))
                .max(Comparator.naturalOrder())
                .orElse(baseDepth);
    }

    int maxWidth(boolean excludeSelf) {

        return Math.max(
                excludeSelf ? 0 : this.children.size(),
                this.children.stream().map(child -> child.maxWidth(false)).max(Comparator.naturalOrder()).orElse(0));
    }

    <R> TreeNode<R> convertSelfAndDescendants(Function<T, R> convertValue) {

        R convertedValue = Optional.ofNullable(this.value).map(convertValue).orElse(null);
        TreeNode<R> convertedSelf = new TreeNode<R>(convertedValue);
        this.children.stream()
                .map(child -> child.convertSelfAndDescendants(convertValue))
                .forEach(convertedSelf::addChild);

        return convertedSelf;
    }

    Collection<T> flatten() {

        return Stream.concat(
                Stream.of(this.value),
                this.children.stream().map(TreeNode::flatten).flatMap(Collection::stream))
                .collect(Collectors.toList());
    }

    String printSelfAndDescendants() {

        return this.value +
                ("\n\n" + this.children.stream()
                        .map(TreeNode::printSelfAndDescendants)
                        .collect(Collectors.joining("\n\n")))
                        .indent(8).stripTrailing();
    }
}
