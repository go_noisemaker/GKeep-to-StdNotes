import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class Note { // "greatest common note"

    String title;

    String text;

    List<String> labels;

    Boolean isArchived;

    OffsetDateTime created;

    OffsetDateTime updated;

    Note(TreeNode<GKeepNode> parentNode, Collection<GKeepLabel> labels) {

        if (parentNode.maxDepth() != 1) {
            throw new IllegalArgumentException(
                    "Node must be parent (expected depth: 1, actual: " + parentNode.maxDepth() + ")");
        }

        // title:      parent
        // text:       children (checklist if multiple)
        // images:     children
        // drawings:   children
        // webLinks:   parent
        // labels:     parent
        // isArchived: parent
        // timestamps: parent

        this.title = parentNode.getValue().getTitle();

        List<GKeepNode> childrenWithText = parentNode.getChildren().stream()
                .map(TreeNode::getValue)
                .filter(node -> node.getText() != null)
                .collect(Collectors.toList());

        if (childrenWithText.size() > 1) { // checklist

            this.text = childrenWithText.stream()
                    .map(node -> "- " + node.getText() + (node.getChecked() ? " - OK" : ""))
                    .collect(Collectors.joining("\n"));

        } else {

            Comparator<GKeepNode> contentSorter = Comparator
                    .comparing(GKeepNode::getText, Comparator.nullsLast(null)) // text takes precedence
                    .thenComparing(GKeepNode::getCreated); // then come other contents by creation date

            Function<GKeepNode, Optional<String>> extractContent = node ->
                    node.getContentType().map(contentType -> {
                        switch (contentType) {
                            case text -> { return node.getText(); }
                            case image -> { return node.getImageInfo(); }
                            case drawing -> { return node.getDrawingInfo(); }
                            default -> throw new RuntimeException("Unexpected content type in child node: " + contentType);
                        }
                    });

            this.text = parentNode.getChildren().stream()
                    .map(TreeNode::getValue)
                    .sorted(contentSorter)
                    .map(extractContent)
                    .flatMap(Optional::stream)
                    .filter(Objects::nonNull)
                    .map(String::trim)
                    .collect(Collectors.joining("\n\n"));
        }

        // add webLinks
        this.text = Stream.concat(
                Stream.ofNullable(this.text),
                Stream.ofNullable(parentNode.getValue().getWebLinks()).flatMap(Collection::stream))
                // optimization: skip text if contained within weblink (e.g. bare URL)
                .reduce((text, webLink) -> webLink.contains(text) ? webLink : (text + "\n\n" + webLink))
                .orElse("");

        this.labels = parentNode.getValue()
                .getLabelIds()
                .stream()
                .map(id -> labels.stream().filter(l -> l.getMainId().equals(id)).findAny().orElseThrow())
                .map(GKeepLabel::getName)
                .collect(Collectors.toList());

        this.isArchived = parentNode.getValue().getIsArchived();

        this.created = parentNode.getValue().getCreated();

        this.updated = Stream.of(parentNode.getValue())
                .flatMap(node -> Stream.of(
                        node.getUpdated(),
                        node.getUserEdited()))
                .max(Comparator.naturalOrder())
                .orElseThrow();
    }
}
