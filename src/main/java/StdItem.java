import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.*;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class StdItem {

    UUID uuid = UUID.randomUUID();

    ContentType contentType;

    Content content;

    OffsetDateTime createdAt;

    OffsetDateTime updatedAt;

    @AllArgsConstructor
    enum ContentType {

        NOTE("Note"),
        SN_ITEMS_KEY("SN|ItemsKey"),
        SN_PRIVILEGES("SN|Privileges"),
        SN_USER_PREFERENCES("SN|UserPreferences"),
        TAG("Tag");

        @JsonValue
        final String s;
    }

    @Getter
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    static class Content {

        Collection<StdItem> references = new ArrayList<>();

        @JsonProperty("appData")
        Map<String, Object> appData = new HashMap<>();

        Boolean isDefault;

        Object previewHtml;

        Object previewPlain;

        String text;

        String title;

        Boolean trashed;
    }
}
