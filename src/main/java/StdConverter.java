import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StdConverter {

    static StdItem convertLabel(GKeepLabel gKeepLabel) {

        StdItem item = new StdItem();

        item.contentType = StdItem.ContentType.TAG;

        item.content = new StdItem.Content();
        item.content.title = gKeepLabel.name;

        item.createdAt = gKeepLabel.created;
        item.updatedAt = gKeepLabel.updated;

        return item;
    }

    static List<StdItem> convertLabels(Collection<GKeepLabel> gKeepLabels) {

        return gKeepLabels.stream()
                .map(StdConverter::convertLabel)
                .collect(Collectors.toList());
    }

    static StdItem convertNote(Note note, Collection<StdItem> tags) {

        StdItem item = new StdItem();

        item.contentType = StdItem.ContentType.NOTE;

        item.content = new StdItem.Content();
        item.content.text = note.text;
        item.content.title = note.title;

        if (note.isArchived) {
            item.content.appData.put(
                    "org.standardnotes.sn", Map.of(
                            "archived", true));
        }

        item.createdAt = note.created;
        item.updatedAt = note.updated;

        // add references to tag items
        tags.stream()
                .filter(tag -> note.labels.contains(tag.content.title))
                .forEach(tag -> {
                    StdItem reference = new StdItem();
                    reference.setUuid(item.uuid);
                    reference.setContentType(StdItem.ContentType.NOTE);
                    tag.content.references.add(reference);
                });

        return item;
    }

    static List<StdItem> convertNotes(Collection<Note> notes, Collection<StdItem> tags) {

        return notes.stream()
                .map(note -> convertNote(note, tags))
                .collect(Collectors.toList());
    }
}
