import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class GKeepLabel {

    String mainId;

    String name;

    OffsetDateTime created;

    OffsetDateTime updated;

    GKeepLabel(JsonNode jsonNode) {

        this.mainId = jsonNode.get("mainId").asText();
        this.name = jsonNode.get("name").asText();
        this.created = OffsetDateTime.parse(jsonNode.get("timestamps").get("created").asText());
        this.updated = OffsetDateTime.parse(jsonNode.get("timestamps").get("updated").asText());
    }
}
