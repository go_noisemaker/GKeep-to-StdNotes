# GKeep-to-StdNotes
Google Keep to Standard Notes converter

## What you need
- Python (for [gkeepapi](https://github.com/kiwiz/gkeepapi))
- Java 14+ (JRE)
- JDK and Maven (if you want to build this thing from source)

## What to do

### Step 0 - Download your Google Keep notes

Dump your Google Keep notes to a JSON file using [gkeepapi](https://github.com/kiwiz/gkeepapi)'s [`Keep.dump()`](https://gkeepapi.readthedocs.io/en/latest/#caching-notes) method.
Call the dump file something like `gknotes.json`.
You can use the following (Python) script as a jump-start:
```
import gkeepapi
import json

keep = gkeepapi.Keep()
keep.login('your_email_address@gmail.com', 'YourPassword')
state = keep.dump()
file = open('gknotes.json', 'w')
json.dump(state, file)
file.close()
```
We're going to use the obtained file as the input for the converter.

### Step 1 - Get a copy of this program

#### Grab a precompiled JAR

For the lazy, check out the Releases section.

#### Build from source

Fire up Maven and produce an executable JAR:
```
mvn package
```
That's it. Pick up the nice JAR that just appeared under the "target" directory. It should be called `gkeep-to-stdnotes-1.1.0.jar` or something similar, depending on the exact version.

### Step 2 - Make it do its job

Launch the executable JAR providing the source file and the destination file as arguments.
If you named your dump `gknotes.json` and want the converter to output `stdnotes.json`:
```
java -jar gkeep-to-stdnotes-1.1.0.jar gknotes.json stdnotes.json
```
It should print a bunch of interesting facts about your notes, and hopefully not blow up with an exception.
If it says "Done", it has probably written your notes to the destination file, in the Standard Notes unencrypted backup format.

### Step 3 - See the results

We're going to feed the converted notes to the Standard Notes app.

1. Go to https://app.standardnotes.org/
2. Open the Account menu from the lower left corner
3. In the Data Backups section, click Import Backup
4. Load `stdnotes.json`
5. Enjoy your converted notes

## Notes

1. It worked for me™. I hope it also works for you™.
2. Images and drawings will not be migrated. Their metadata (name, type, size) will be written to their containing notes' text, trailing any other contents.
3. Web links will also appear under the note's text, with page title and URL.
4. Creation and modification dates should be preserved.
5. Tags are preserved.
6. The Standard Notes format in use is the latest one at the moment of writing, versioned "004".
7. If either of Google Keep or Standard Notes change their format to something uncompatible, tough luck, I'm afraid. Better convert your notes while this still works.
